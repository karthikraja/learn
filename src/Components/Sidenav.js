import React from 'react'
import './Sidenav.css';

class Sidenav extends React.Component {
    render() {
        return (
                
            <nav class="main-menu" style={{ backgroundColor:"#ffffff"}}>
                <br/>
                <div className="text-center">
                <a href="/">
                    <img src="./images/logo1.png" width="30" height="30" alt=""/></a>
                    <p style={{ color: "#000000"}}>LOGO</p>
                </div>
            <ul>
                <li>
                    <a href="#/">
                        <i class="far fa-play-circle fa fa-2x"></i>
                        <span class="nav-text">
                            Courses
                        </span>
                    </a>
                  
                </li>
                <li class="has-subnav">
                    <a href="#/">
                        <i class="fa fa-laptop fa-2x"></i>
                        <span class="nav-text">
                            Code Kata
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="#/">
                       <i class="fa fa-list fa-2x"></i>
                        <span class="nav-text">
                            Micro Arc
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="#/">
                       <i class="fa fa-folder-open fa-2x"></i>
                        <span class="nav-text">
                            Project Board
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="#/">
                        <i class="fa fa-bar-chart-o fa-2x"></i>
                        <span class="nav-text">
                            Lead Board
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#/">
                        <i class="fa fa-font fa-2x"></i>
                        <span class="nav-text">
                           Rewards
                        </span>
                    </a>
                </li>
                <li>
                   <a href="#/">
                       <i class="fa fa-table fa-2x"></i>
                        <span class="nav-text">
                            Jobs
                        </span>
                    </a>
                </li>
            </ul>

            <ul class="logout">
                <li>
                <a href="#/">
                <i class="fa fa-t"></i>
                <span class="nav-link" href="#/">Zen Class</span></a>
                </li>
                <li>
                <a href="#/">
                <i class="fa fa-t"></i>
                <span class="nav-link" href="#/">HYRE</span></a>
                </li>
                <li>
                <a href="#/">
                <i class="fa fa-t"></i>
                <span class="nav-link" href="#/">Our Story</span></a>
                </li>
                <li>
                <a href="#/">
                <i class="fa fa-t"></i>
                <span class="nav-link" href="#/">Terms and Conditions</span></a>
                </li>
            </ul>
        </nav>

            )
        }
    }
    
    export default (Sidenav);