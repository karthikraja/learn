import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";

const Loading = () => <div>Loading...</div>;

const Home = Loadable({
  loader: () => import("./Containers/Home"),
  loading: Loading
});

const Courses = Loadable({
  loader: () => import("./Containers/Courses"),
  loading: Loading
});

const Courses1 = Loadable({
  loader: () => import("./Containers/Courses1"),
  loading: Loading
});

const Courses2 = Loadable({
  loader: () => import("./Containers/Courses2"),
  loading: Loading
});


const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/courses" component={Courses} />
      <Route exact path="/courses1" component={Courses1} />
      <Route exact path="/courses2" component={Courses2} />
    </Switch>
  </BrowserRouter>
);

export default Routes;