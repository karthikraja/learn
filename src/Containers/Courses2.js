import React from 'react'
import Courses from './Courses'

class Courses2 extends React.Component {
    render() {
        return (
            <div>
                <Courses/>
                <br/>
                <div style={{ paddingLeft: "90px"}}>
                
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg" style={{ maxWidth:"350px", height:"auto" }}  alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>Advanced Java</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg" style={{ maxWidth:"350px", height:"auto" }}  alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>After Effect</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg"  style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>Angular JS</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>CSS Grid and Fluxbox</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>javascript AJAX</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/14.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>HTML 5 Canvas</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    
                </div>  
            </div>
            )
        }
    }

    
    
export default (Courses2);