import React from 'react'
import Sidenav from '../Components/Sidenav'

class Courses extends React.Component {
    render() {
        return (
            <div>
                <Sidenav/>
                <div style={{paddingLeft: "50px"}}>
                    <nav class="navbar navbar-expand-lg navbar-light shadow p-3 mb-5 bg-white">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item active">
                                        </li>
                                    </ul>

                                    <a class="navbar-item" href="#/" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px", color:"#4a2aff"}}>
                                        <div>SIGN IN</div>
                                    </a>
                                    <a class="navbar-item" href="#/" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px", color:"#4a2aff"}}>
                                        <div>REGISTER</div>
                                    </a>

                            </div>
                        </nav>
                </div>
                <div style={{ paddingLeft: "90px"}}>
                    <h3>Courses</h3>
                    <div className ="text">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" style={{ color: "#a0a0a0"}} href="/Courses1">Course Library</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style={{ color: "#a0a0a0"}} href="/Courses2">Free Courses</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                )
        }
    }
export default (Courses);