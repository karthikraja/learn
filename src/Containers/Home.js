import React from 'react'
import './Home.css';

class Home extends React.Component {
    render() {
        return (
            <div>
                <nav class="navbar navbar-expand-lg navbar-light shadow p-3 mb-5 bg-white">
                <a class="navbar-brand" href="#/">
                    <img src="./images/logo1.png" width="30" height="30" class="d-inline-block align-top" alt=""/>
                </a>
                <a class="navbar-brand" href="#/" style={{ fontSize:"24px"}}>
                    LOGO
                </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                        </li>
                        </ul>

                        <ul class="navbar-nav">
                        <li class="nav-item" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                        <a class="nav-link" href="/courses1"> COURSES </a>
                        </li>
                        <li class="nav-item" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                        <a class="nav-link" href="#/"> PRACTICE </a>
                        </li>
                        <li class="nav-item" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                        <a class="nav-link" href="#/"> HYRE </a>
                        </li>
                        <li class="nav-item" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                        <a class="nav-link" href="#/"> ZEN CLASS </a>
                        </li>
                        <li class="nav-item" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                        <a class="nav-link" href="#/"> OUR STORY </a>
                        </li>
                        </ul>
                        
                        <a class="navbar-item" href="#/" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px"}}>
                            <div style={{ borderLeft: "1px solid #BEBCBC",  height: "30px"}}></div>
                        </a>
                        <a class="navbar-item" href="#/" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px", color:"#4a2aff"}}>
                            <div>SIGN IN</div>
                        </a>
                        <a class="navbar-item" href="#/" style={{ paddingTop:"12px", paddingLeft:"16px", paddingRight:"16px", paddingBottom: "12px", color:"#4a2aff"}}>
                            <div>REGISTER</div>
                        </a>

                    </div>
                </nav>

                <br/>
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <br />
                        <br />
                        <h2 style={{color: "#5c88f5"}}>Learn Practice Get Hired</h2>
                        <p style={{color: "#525558"}}>Join over 1.4 lakh+ programmers.</p>
                        <p style={{color: "#525558"}}>Trusted by 200+ top companies and 100+ colleges.</p>
                        <br/>
                        <div>
                            <button type="button" class="button learn btn" ><span>To Learn</span></button>
                            <button type="button" class="button learn btn" ><span>To Hire</span></button>
                        </div>
                        <br />
                        <br />
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid sky" src="./images/learn1.png" style={{width: "100%"}} alt="..."></img>
                        </div>
                    </div>
                </div>
            <br/>
            <br/>
            <div className="text-center">
                <h2>Why Learn/Hire from LOGO?</h2>
            </div>
            
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <br />
                        <br />
                        <br/>
                        <h2 style={{color: "#5c88f5"}}>Learn and Practise<br/>Programming with courses<br/>created by industry experts</h2>
                        <br />
                        <p style={{color: "#525558"}}>Learn to code in your own native language (English, Tamil, Telugu,Hindi)</p>
                        <br />
                        <br />
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid sky" src="./images/learn2.png" style={{width: "100%"}} alt="..."></img>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container" style={{ width: "75%"}}>
                    <div class="row">
                        <div class="col-sm text-center">
                        <img class="img" src="./images/10x.png" style={{ width:"135px"}} alt="..."></img>
                        <br/>
                        <br/>
                        <div style={{fontSize:"24px"}}>72%</div>Learners complete their course less 3 month duration<br/><br/>
                        </div>
                        <div class="col-sm text-center">
                        <img class="img" src="./images/data.png" style={{ width:"75px"}} alt="..."></img>
                        <br/>
                        <br/>
                        <div style={{fontSize:"24px"}}>78%</div>Learners able to recollect & related faster<br/><br/>
                        </div>
                        <div class="col-sm text-center">
                        <img class="img" src="./images/understand.png" style={{ width:"76px"}} alt="..."></img>
                        <br/>
                        <br/>
                        <div style={{fontSize:"24px"}}>84%</div>Learners have better understanding of complex concept<br/><br/>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <br />
                        <br />
                        <br/>
                        <h2 style={{color: "#5c88f5"}}>Codekata</h2>
                        <p style={{color: "#525558"}}>Practise coding in incremental difficulty levels. Codekata is a gamified practice platform which hosts 10000+ curated coding problems.</p>
                        <br />
                        <br />
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid sky" src="./images/learn3.png" style={{width: "100%"}} alt="..."></img>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="./images/micro.png" style={{width: "100%" }} alt="..."></img>
                        </div>
                        <div class="col-md-6">
                        <br/>
                        <br/>
                        <br/>
                        <div className="container" style={{ paddingLeft: "20px"}}>
                        <h2 style={{color: "#5c88f5"}}> Micro Arc Test</h2>
                        <p style={{color: "#525558"}}>Test how good you are in tech skills when compared<br/>to your peers. ARC is an interactive skill assessment<br/>which helps you to test your skills.</p>
                        </div>
                        <br />
                        <br />
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <br />
                        <br />
                        <br/>
                        <h2 style={{color: "#5c88f5"}}>Build your Skill profile</h2>
                        <br />
                        <p style={{color: "#525558"}}>Build your personal brand by building your skill profile.Its a<br/>great way to tell recruiters who you are and what skills you<br/>have.</p>
                        <br />
                        <br />
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid sky" src="./images/skill.png" style={{width: "100%"}} alt="..."></img>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="./images/micro.png" style={{width: "100%" }} alt="..."></img>
                        </div>
                        <div class="col-md-6">
                        <br/>
                        <br/>
                        <br/>
                        <div className="container" style={{ paddingLeft: "20px"}}>
                        <h2 style={{color: "#5c88f5"}}>Zen class</h2>
                        <p style={{color: "#6f06f5"}}>Coding Bootcamp . New Age GURUKULAM</p>
                        <p style={{color: "#525558"}}>3 months Intensive Project Based Learning program, where<br/>you will learn and gain a skill ( code like wizard from Hogwarts )<br/>with mentors from industry solving real world problems</p>
                        </div>
                        <br />
                        <br />
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <div className="text-center">
                    <h2>How it works</h2>
                </div>
                <br/>
                <br/>

                <div class="container">
            <div class="row blog">
                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#blogCarousel" data-slide-to="1"></li>
                        </ol>
                        
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row text-center">
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Join</h4>
                                          <p class="card-text">Create your GUVI account<br/>Build your profile, Get interview calls from companies and land in your dream job.</p>
                                          <button class="btn btn-primary back">Create</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Explore</h4>
                                          <p class="card-text">Explore a variety of courses in different languages and technologies and choose one to start with.</p>
                                          <button class="btn btn-primary back">Explore</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Get Personalised suggestion</h4>
                                          <p class="card-text">Take a pre assessment, Know where you stand and get personalised suggestions on course topics.</p>
                                          <button class="btn btn-primary back">Learn More</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(60).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Get Certified</h4>
                                          <p class="card-text">Take a Certification test after learning and get certified by GUVI.</p>
                                          <button class="btn btn-primary back">Learn More</button>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                               
                            </div>
                            
                            <div class="carousel-item">
                                <div class="row text-center">
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Card title</h4>
                                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                          <button class="btn btn-primary back">Button</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Card title</h4>
                                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                          <button class="btn btn-primary back">Button</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Card title</h4>
                                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                          <button class="btn btn-primary back">Button</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="card">
                                        <img class="card-img-top"
                                          src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg"
                                          alt=""/>
                                        <div class="card-body">
                                          <h4 class="card-title">Card title</h4>
                                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                          <button class="btn btn-primary back">Button</button>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                       
                      </div>
      
                    </div>
                  </div>
                </div>
                <br/>

                <br/>
                <br/>
                <br/>
                <br/>
                <div className="text-center">
                    <h2>Hiring Partners</h2>
                </div>

                <br/>
                <br/>
                <div class="container">
                  <div className ="row text-center">
                    <div className ="col-6">
                        <div class="row">
                          <div class="col-md-4 ">
                                  <img src="./images/advocately.png" style={{width: "100px"}} alt="..." />
                                  <br/>
                                  <br/>
                              </div>
                              <div class="col-md-4 ">
                                  <img src="./images/freshworks.png" style={{width: "100px"}} alt="..." />
                                  <br/>
                                  <br/>
                              </div>
                              <div class="col-md-4 ">
                                  <img src="./images/okta.png" style={{width: "80px"}} alt="..." />
                                  <br/>
                                  <br/>
                              </div>
                        </div>
                    </div>
                    <div className ="col-6">
                      <div class="row">
                            <div class="col-md-4 ">
                                <img src="./images/paypal.png" style={{width: "100px"}} alt="..." />
                                <br/>
                                <br/>
                            </div>
                            <div class="col-md-4 ">
                                <img src="./images/worldpay.png" style={{width: "100px"}} alt="..." />
                                <br/>
                                <br/>
                            </div>
                            <div class="col-md-4 ">
                                <img src="./images/envoy.png" style={{width: "100px"}} alt="..." />
                                <br/>
                                <br/>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
                <br/>

                <br/>
                <br/>
                <div className="text-center">
                    <h2>Our Journey so Far</h2>
                </div>

                <br/>
                <br/>
                <br/>
                <div class="container">
                    <div class="row">
                    <div class="col-md-6">
                        <br />
                        <br />
                        <div class="container">
                          <div class="row">
                            
                            <div class="col-4">
                              <div className="card" style={{ padding: "10px", borderColor:"#303030"}}>
                                <p>156795<br/><span style={{color:"#6f06f5"}}>users</span></p>
                              </div>
                            </div>
                            
                            <div class="col-6">
                              <div className="card" style={{ padding: "10px", borderColor:"#303030"}}>
                                <p>194<br/><span style={{color:"#6f06f5"}}>Members</span></p>
                              </div>
                            </div>

                          </div>
                          
                          <br/>

                          <div class="row">
                          
                            <div class="col-6">
                              <div className="card" style={{ padding: "10px", borderColor:"#303030"}}>
                                <p>1168546<br/><span style={{color:"#6f06f5"}}>Lines of code submissions</span></p>
                                </div>
                              </div>
                            
                            <div class="col-4">
                              <div className="card" style={{ padding: "10px", borderColor:"#303030"}}>
                                <p>1673<br/><span style={{color:"#6f06f5"}}>videos</span></p>
                              </div>
                            </div>
                          </div>
                          <br/>
                          <br/>
                        </div>
  
                        </div>
                        <div class="col-md-6">
                            <img class="img-fluid sky" src="./images/office.jpg" style={{width: "100%", borderRadius:"5px"}} alt="..."></img>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <br/>
                <br/>
                <div className="container" style={{ maxWidth:"70%"}}>
                  <div class="card back" style={{ borderRadius: "10px"}}>
                    <div class="card-body">
                      <h4 style={{ color:"#b6b6b6"}}>Sign up for Free courses & Workshop.</h4>
                      
                      <br/>
                      
                      <form>
                      <div class="form-group row">
                        <div class="col-sm-6">
                          <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="email@example.com"></input>
                          <br/>
                        </div>
                        <div className="col-sm-2 col-form">
                          <button class="btn btn-success ">Subscribe</button>
                        </div>
                      </div>
                      </form>

                    </div>
                  </div>
                </div>
                <br/>
                <br/>

<footer class="page-footer font-small stylish-color-dark pt-4" style={{ backgroundColor:"#f7f7f7"}}>

  <div class="container text-center text-md-left">

    <div class="row">


      <div class="col-md-4 mx-auto">

        <img src="./images/logo1.png" width="30" height="30" class="d-inline-block align-top" alt=""/>
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4" style={{ color: "#3f74ff"}}>LOGO</h5>
        <p>Tech deserves you</p>

      </div>

      <hr class="clearfix w-100 d-md-none"/>

    
      <div class="col-md-2 mx-auto">

    
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">SOLUTION</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#/" style={{ color: "#777777"}}>Personal</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Classes</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Corporate</a>
          </li>
          <br/>
        </ul>

      </div>
    

      <hr class="clearfix w-100 d-md-none"/>

      
      <div class="col-md-2 mx-auto">

       
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">PLATFORM</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#/" style={{ color: "#777777"}}>Course library</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Codekata</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>MicroArc</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Project board</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>HYRE</a>
          </li>
          <br/>
        </ul>

      </div>
      

      <hr class="clearfix w-100 d-md-none"/>

      
      <div class="col-md-2 mx-auto">

        
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">COMPANY</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#/" style={{ color: "#777777"}}>About Us</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Contact us</a>
          </li>
          <br/>
          <li>
            <a href="#/" style={{ color: "#777777"}}>Terms and Conditions</a>
          </li>
          <br/>
        </ul>

      </div>
    

    </div>
    

  </div>
  

  <hr/>

  
  <ul class="list-unstyled list-inline text-center py-2">
    <li class="list-inline-item">
      <h5 class="mb-1">Register for free </h5>
    </li>
    <li class="list-inline-item">
      <a href="#/" class="btn learn btn-rounded"><span style={{color:"#ffffff"}}> Sign up!</span></a>
    </li>
  </ul>
  

  <hr/>

  
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
      <button class="btn" style={{ borderRadius:"30px", height:"50px", width:"50px", backgroundColor:"#3b5998"}}>
        <i class="fab fa-facebook-f" style={{ color:"#ffffff"}}> </i>
      </button>
    </li>
    <li class="list-inline-item">
    <button class="btn" style={{ borderRadius:"30px", height:"50px", width:"50px", backgroundColor:"#55acEE"}}>
        <i class="fab fa-twitter" style={{ color:"#ffffff"}}> </i>
      </button>
    </li>
    <li class="list-inline-item">
    <button class="btn" style={{ borderRadius:"30px", height:"50px", width:"50px", backgroundColor:"#dd4b39"}}>
        <i class="fab fa-google-plus-g" style={{ color:"#ffffff"}}> </i>
      </button>
    </li>
    <li class="list-inline-item">
    <button class="btn" style={{ borderRadius:"30px", height:"50px", width:"50px", backgroundColor:"#0082ca"}}>
        <i class="fab fa-linkedin-in" style={{ color:"#ffffff"}}> </i>
      </button>
    </li>
    <li class="list-inline-item">
    <button class="btn" style={{ borderRadius:"30px", height:"50px", width:"50px", backgroundColor:"#ec4a89"}}>
        <i class="fab fa-dribbble" style={{ color:"#ffffff"}}> </i>
      </button>
    </li>
  </ul>
  

  
  <div class="footer-copyright text-center py-3">© 2019 :
    <a href="#/"> example@example.com</a>
  </div>
  
</footer>
            </div>
            )
        }
    }
    
export default Home;