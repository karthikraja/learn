import React from 'react'
import Courses from './Courses'

class Courses1 extends React.Component {
    render() {
        return (
            <div>
                <Courses/>
                <br/>
                <div style={{ paddingLeft: "90px"}}>
                
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg" style={{ maxWidth:"350px", height:"auto" }}  alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>Deep Learning</p>
                                    <p className="card-text">Bundle of 13 courses / English</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 20/-</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg" style={{ maxWidth:"350px", height:"auto" }}  alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>C</p>
                                    <p className="card-text">26 Exclusive Lessons / 39 hrs / Tamil</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 22/-</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg"  style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>C plus plus</p>
                                    <p className="card-text">39 Exclusive Lessons / 59 hrs / Tamil</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 25/-</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>HTML & CSS</p>
                                    <p className="card-text">30 Exclusive Lessons / 45 hrs / Tamil</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 20/-</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>Java 8</p>
                                    <p className="card-text">30 Exclusive Lessons / 68 hrs / Tamil</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 20/-</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card" style={{ maxWidth: "350px"}}>
                                <img src="./images/office.jpg" style={{ maxWidth:"350px", height:"auto" }} alt="..."/>
                                <div class="card-body">
                                    <p class="card-text" style={{ fontWeight:"bold"}}>Java</p>
                                    <p className="card-text">67 Exclusive Lessons / 101 hrs / English</p>
                                    <button className="btn btn-success" style={{ float:"right"}}>Enroll</button>
                                    <br/>
                                    <br/>
                                    <a href="#/" style={{ float:"left"}}>Course details</a>
                                    <p style={{ float:"right"}}>$ 20/-</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    
                </div>  
            </div>
            )
        }
    }

    
    
export default (Courses1);